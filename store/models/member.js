import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  members: [],
}

export const counterSlice = createSlice({
  name: "members",
  initialState,
  reducers: {
    addMember: (state, action) => {
      state.members.push(action.payload);
    },
    editMember: (state, action) => {
      state.members = action.payload
    },
    deleteMember: (state, action) => {
      state.members = state.members.filter((item) => item.id !== action.payload)
    },
  },
});

// Action creators are generated for each case reducer function
export const { addMember, editMember, deleteMember } = counterSlice.actions;

export default counterSlice.reducer;

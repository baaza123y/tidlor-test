import React from "react";
import { StatusBar } from 'expo-status-bar';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from './src/Home';
import AddMember from './src/AddMember';
import EditMember from './src/EditMember';

import store from './store';
import {Provider} from 'react-redux';

function App() {
  const Stack = createNativeStackNavigator();

  return (
    <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="AddMember" component={AddMember} />
        <Stack.Screen name="EditMember" component={EditMember} />
      </Stack.Navigator>
    </NavigationContainer>
    </Provider>
  );
}

export default App;

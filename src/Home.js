import React, { useCallback,useState } from "react";
import { StatusBar } from "expo-status-bar";
import {
  FlatList,
  View,
  Text,
  Modal as ModalRN,
  StyleSheet,
  Button,
} from "react-native";
import Container from "../components/Container";
import Header from "../components/Header";
import ListItem from "../components/ListItem";
import AddItem from "../components/AddItem";
import { useSelector,useDispatch } from "react-redux";

import { deleteMember } from "../store/models/member";

const Home = ({ navigation }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [item, setItem] = useState([]);
  const member = useSelector(state => state.member.members)  
  const dispatch = useDispatch()
  
  console.log('membe1111r',member)

  return (
    <Container>
      <Header title="My Members" />
      <View>
        <FlatList
          style={{ paddingBottom: 15 }}
          data={member}
          renderItem={({ item }) => (
            <ListItem
              item={item}
              setItem={setItem}
              setIsOpen={setIsOpen}
              onPress={() => navigation.navigate('EditMember', {
                memberId: item.id,
              })}
              
            />
          )}
          keyExtractor={(item) => item.id}
        />
          <View style={{ paddingLeft: 15, paddingRight: 15 }}>
            <AddItem onPress={() => navigation.navigate("AddMember")} />
          </View>
      </View>
      <ModalRN
        visible={isOpen}
        animationType="fade"
        onRequestClose={() => setIsOpen(false)}
        transparent
      >
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: "rgba(0,0,0,0.4)",
          }}
        >
          <>
            <View style={style.modalView}>
              <View style={{ alignItems: "center", paddingBottom: 20 }}>
                <Text style={style.TextTitle}>คุณต้องการลบข้อมูลหรือไม่?</Text>
                <Text style={style.Text}>{item.name}</Text>
              </View>
              <View
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-around",
                }}
              >
                <Button
                  style={{
                    width: "50%",
                  }}
                  onPress={() => {
                    setIsOpen(false);
                  }}
                  title={"ยกเลิก"}
                />
                <Button
                  style={{ width: "50%" }}
                  onPress={() => {
                    setIsOpen(false), dispatch(deleteMember(item.id));
                  }}
                  title={"ตกลง"}
                />
              </View>
            </View>
          </>
        </View>
      </ModalRN>
    </Container>
  );
};

const style = StyleSheet.create({
  modalView: {
    display: "flex",
    justifyContent: "space-between",
    margin: 20,
    backgroundColor: "white",
    borderRadius: 6,
    padding: 20,
    width: "90%",
    // alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  TextTitle: {
    fontSize: 20,
    paddingBottom: 8,
  },
  Text: {
    fontSize: 18,
  },
});

export default Home;

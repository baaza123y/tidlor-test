import { StatusBar } from "expo-status-bar";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import Container from "../components/Container";
import Header from "../components/Header";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { editMember } from "../store/models/member";
import { mask } from "react-native-mask-text";

const EditMember = ({ route, navigation }) => {
  const param = route.params;
  const { control, handleSubmit } = useForm();
  const dispatch = useDispatch();
  const member = useSelector(
    (state) => state.member.members.find((item) => item.id === param.memberId)
  );
  const members = useSelector(state => state.member.members)  

  return (
    <Container>
      <Header
        back
        onPress={() => navigation.goBack()}
        title="Edit New Member"
      />
      <View style={style.container}>
        <View style={style.form}>
          <Text style={style.label}>NAME </Text>
          <Controller
            name="name"
            control={control}
            defaultValue={member.name}
            // rules={{ required: "This is required." }}
            render={({ field: { onChange, value } }) => (
              <TextInput
                style={style.input}
                placeholder="NAME"
                onChangeText={(value) => {
                  onChange(value);
                }}
                value={value}
              />
            )}
          />
        </View>
        <View style={style.form}>
          <Text style={style.label}>CARD </Text>
          <Controller
            name="card"
            control={control}
            defaultValue={member.card}
            // rules={{ required: "This is required." }}
            render={({ field: { onChange, value } }) => (
              <TextInput
                style={style.input}
                placeholder="CARD"
                onChangeText={(value) =>
                  onChange(value ? mask(value, "9-9999-99999-99-9") : value)
                }
                value={value}
              />
            )}
          />
        </View>
        <View style={style.form}>
          <Text style={style.label}>PHONE Number </Text>
          <Controller
            name="phone"
            control={control}
            // rules={[
            //   {
            //     required: true,
            //     message: "This is required.",
            //   },
            // ]}
            defaultValue={member.phone}
            render={({ field: { onChange, value } }) => (
              <TextInput
                style={style.input}
                placeholder="PHONE NUMBER"
                onChangeText={(value) =>
                  onChange(value ? mask(value, "999-999-9999") : value)
                }
                value={value}
              />
            )}
          />
        </View>

        <TouchableOpacity
          onPress={handleSubmit((data) => {
            const edited = {
              id: member.id,
              name: data.name, 
              card: data.card ,
              phone: data.phone,
            }
            const newMembers = members.map((item) => {
              if(item.id === edited.id){
                return edited
              }
              return item
            })
            dispatch(
              editMember(newMembers)
            ),
              navigation.navigate("Home");
          })}
          style={style.btn}
        >
          <View>
            <Text type="submit" style={style.btnText}>
              Submit
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </Container>
  );
};

const style = StyleSheet.create({
  container: {
    padding: 20,
  },
  form: {
    marginBottom: 20,
  },
  label: {
    margin: 10,
    marginLeft: 0,
  },
  input: {
    backgroundColor: "white",
    height: 40,
    padding: 10,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "#1E90FF",
  },
  btn: {
    backgroundColor: "#1E90FF",
    marginTop: 20,
    padding: 2,
  },
  btnText: {
    color: "white",
    padding: 10,
    fontSize: 18,
    textAlign: "center",
    backgroundColor: "#1E90FF",
  },
});

export default EditMember;

import { StatusBar } from "expo-status-bar";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import Container from "../components/Container";
import Header from "../components/Header";
import { Controller, useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { addMember } from "../store/models/member";
import { mask } from "react-native-mask-text";
import uuid from "uuid-random";

const AddMember = ({ navigation }) => {
  const { control, handleSubmit } = useForm();
  const dispatch = useDispatch();

  return (
    <Container>
      <Header back onPress={() => navigation.goBack()} title="Add New Member" />
      <View style={style.container}>
        <View style={style.form}>
          <Text style={style.label}>NAME </Text>
          <Controller
            name="name"
            control={control}
            rules={{ required: "This is required." }}
            render={({ field: { onChange, value } }) => (
              <TextInput
                style={style.input}
                placeholder="NAME"
                onChangeText={(value) => {
                  onChange(value);
                }}
                value={value}
              />
            )}
          />
        </View>
        <View style={style.form}>
          <Text style={style.label}>CARD </Text>
          <Controller
            name="card"
            control={control}
            rules={{ required: "This is required." }}
            render={({ field: { onChange, value } }) => (
              <TextInput
                style={style.input}
                placeholder="CARD"
                onChangeText={(value) =>
                  onChange(value ? mask(value, "9-9999-99999-99-9") : value)
                }
                value={value}
              />
            )}
          />
        </View>
        <View style={style.form}>
          <Text style={style.label}>PHONE Number </Text>
          <Controller
            name="phone"
            control={control}
            rules={[
              {
                required: true,
                message: "This is required.",
              },
            ]}
            render={({ field: { onChange, value } }) => (
              <TextInput
                style={style.input}
                placeholder="PHONE NUMBER"
                onChangeText={(value) =>
                  onChange(value ? mask(value, "999-999-9999") : value)
                }
                value={value}
              />
            )}
          />
        </View>

        <TouchableOpacity
          onPress={handleSubmit((data) => {
            dispatch(
              addMember({
                id: uuid(),
                name: data.name,
                card: data.card,
                phone: data.phone,
              })
            ),
              navigation.navigate("Home");
          })}
          style={style.btn}
        >
          <View>
            <Text type="submit" style={style.btnText}>
              Submit
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </Container>
  );
};

const style = StyleSheet.create({
  container: {
    padding: 20,
  },
  form: {
    marginBottom: 20,
  },
  label: {
    margin: 10,
    marginLeft: 0,
  },
  input: {
    backgroundColor: "white",
    height: 40,
    padding: 10,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "#1E90FF",
  },
  btn: {
    backgroundColor: "#1E90FF",
    marginTop: 20,
    padding: 2,
  },
  btnText: {
    color: "white",
    padding: 10,
    fontSize: 18,
    textAlign: "center",
    backgroundColor: "#1E90FF",
  },
});

export default AddMember;

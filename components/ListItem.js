import React from "react";
import { StatusBar } from "expo-status-bar";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import EditIcon from "../assets/icon/edit.png";
import DeleteIcon from "../assets/icon/delete.png";

const ListItem = ({ item, setItem, setIsOpen, onPress }) => {
  return (
    <View style={style.listItem}>
      <View style={style.item}>
        <View>
          <Text style={style.textStart}>{item.name}</Text>
          <Text style={style.textCenter}>{item.card} </Text>
          <Text style={style.textEnd}>{item.phone} </Text>
        </View>
        <View style={style.itemLeft}>
          <TouchableOpacity
            onPress={() => 
              setItem(item),
              onPress
            }
          >
            <Image
              source={EditIcon}
              resizeMode="cover"
              style={{
                width: 30,
                height: 30,
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              setItem(item), setIsOpen(true);
            }}
          >
            <Image
              source={DeleteIcon}
              resizeMode="cover"
              style={{
                width: 30,
                height: 30,
              }}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  listItem: {
    flex: 1,
    paddingLeft: 15,
    paddingTop: 15,
    paddingRight: 15,
  },
  item: {
    backgroundColor: "#1E90FF",
    padding: 20,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  itemLeft: {
    justifyContent: "space-between",
  },
  textStart: {
    fontSize: 20,
    color: "white",
    marginBottom: 8,
  },
  textCenter: {
    fontSize: 18,
    color: "white",
    marginBottom: 8,
  },
  textEnd: {
    fontSize: 18,
    color: "white",
    marginBottom: 8,
  },
  icon: {
    width: 25,
    height: 25,
    marginTop: 8,
  },
});

export default ListItem;

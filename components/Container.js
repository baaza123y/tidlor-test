import { StatusBar } from 'expo-status-bar';
import {View, StyleSheet} from 'react-native';

const Container = ({children}) => {
  return <View style={style.container}>{children}</View>;
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default Container;

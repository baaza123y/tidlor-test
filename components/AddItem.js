import { StatusBar } from "expo-status-bar";
import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import PlusIcon from "../assets/icon/plus.png";

const AddItem = ({ addItem, onPress }) => {
  return (
    <View>
      <TouchableOpacity onPress={onPress} style={style.btn}>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            backgroundColor: "white",
            justifyContent: "center",
          }}
        >
          <Image
            source={PlusIcon}
            resizeMode="cover"
            style={{
              width: 25,
              height: 25,
              marginTop:7
            }}
          />
          <Text style={style.btnText}>Add member</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const style = StyleSheet.create({
  btn: {
    backgroundColor: "#1E90FF",
    padding: 2,
  },
  btnText: {
    color: "#1E90FF",
    padding: 10,
    fontSize: 18,
    textAlign: "center",
    backgroundColor: "white",
  },
});

export default AddItem;

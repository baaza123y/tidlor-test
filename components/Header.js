import { StatusBar } from "expo-status-bar";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import ArrowIcon from "../assets/icon/ArrowIcon.png";

const Header = ({ title, back, onPress }) => {
  return (
    <View style={style.header}>
      <View
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        {back && (
          <View
            style={{
              flex: 0.5,
              justifyContent: "flex-start",
              flexDirection: "row",
            }}
          >
            <TouchableOpacity onPress={onPress}>
              <Image
                source={ArrowIcon}
                resizeMode="cover"
                style={{
                  width: 30,
                  height: 30,
                }}
              />
            </TouchableOpacity>
            {/* <Text style={style.textback}>Back</Text> */}
          </View>
        )}

        <Text
          style={{
            flex: 1,
            color: "#fff",
            fontSize: 18,
            textAlign: !back ? "center" : "left",
          }}
        >
          {title}
        </Text>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  header: {
    height: 100,
    padding: 15,
    backgroundColor: "#1E90FF",
    justifyContent: "flex-end",
  },
  textback: {
    flex: 1,
    color: "#fff",
    fontSize: 18,
    marginTop: 4,
  },
  text: {},
});

export default Header;
